#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-test
code ~/projects/stam/L03/unit-test

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore    

git remote add origin https://gitlab.com/Fakiiri/unit-test
git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

node --watch-path=src src/main.js

npm run test

git add . --dry-run
git status
git push
git push --set-upstream origin main
git remote -v