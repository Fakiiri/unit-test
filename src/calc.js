// TODO: arithmetic operations
/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a+b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
}
/**
 * Diivides two numbers
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error}
 */
const divide = (dividend, divisor) => {
    if(divisor == 0) throw new Error("0 division not allowed")
        const fraction = dividend / divisor;
        return fraction;

}

export default { add, subtract, multiply, divide }
